import dbtokens.Group;
import dbtokens.School;
import dbtokens.Subject;
import dbtokens.Trainee;
import org.junit.Test;
import visitor.DeleteVisitor;
import visitor.InsertVisitor;
import visitor.Visitor;

public class TestDataBaseService {
    @Test
    public void testInsertDataBaseRequest(){
        Trainee trainee = new Trainee(6, "Nick", "Axe", 4);
        Subject subject = new Subject(4, "Math");
        School school = new School(5, "School 64", 1996);
        Group group = new Group(3, "A3", "215a");

        Visitor visitor = new InsertVisitor();

        System.out.println(trainee.accept(visitor));
        System.out.println(subject.accept(visitor));
        System.out.println(school.accept(visitor));
        System.out.println(group.accept(visitor));
    }

    @Test
    public void testDeleteDataBaseRequest(){
        Trainee trainee = new Trainee(6, "Nick", "Axe", 4);
        Subject subject = new Subject(4, "Math");
        School school = new School(5, "School 64", 1996);
        Group group = new Group(3, "A3", "215a");

        Visitor visitor = new DeleteVisitor();

        System.out.println(trainee.accept(visitor));
        System.out.println(subject.accept(visitor));
        System.out.println(school.accept(visitor));
        System.out.println(group.accept(visitor));
    }
}
