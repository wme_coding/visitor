package dbtokens;

import visitor.Visitor;

public abstract class DataBaseToken {
    public abstract String accept(Visitor visitor);
}
