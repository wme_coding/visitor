package visitor;

import dbtokens.Group;
import dbtokens.School;
import dbtokens.Subject;
import dbtokens.Trainee;

public class InsertVisitor implements Visitor{

    @Override
    public String visitTrainee(Trainee trainee) {
        return String.format("insert into trainee values(%d,%s,%s,%d)",
                trainee.getId(),
                trainee.getFirstName(),
                trainee.getLastName(),
                trainee.getRating());
    }

    @Override
    public String visitSubject(Subject subject) {
        return String.format("insert into subject values(%d,%s)",
                subject.getId(),
                subject.getName());
    }

    @Override
    public String visitSchool(School school) {
        return String.format("insert into school values(%d,%s,%d)",
                school.getId(),
                school.getName(),
                school.getYear());
    }

    @Override
    public String visitGroup(Group group) {
        return String.format("insert into `group` values(%d,%s,%s);",
                group.getId(),
                group.getName(),
                group.getRoom());
    }
}
