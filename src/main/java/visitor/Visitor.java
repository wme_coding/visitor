package visitor;

import dbtokens.Group;
import dbtokens.School;
import dbtokens.Subject;
import dbtokens.Trainee;

public interface Visitor {
    String visitTrainee(Trainee trainee);
    String visitSubject(Subject subject);
    String visitSchool(School school);
    String visitGroup(Group group);
}
