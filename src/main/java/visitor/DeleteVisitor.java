package visitor;

import dbtokens.Group;
import dbtokens.School;
import dbtokens.Subject;
import dbtokens.Trainee;

public class DeleteVisitor implements Visitor{
    @Override
    public String visitTrainee(Trainee trainee) {
        return String.format("delete from trainee where id = %d and firstname = %s and lastname = %s and rating = %d",
                trainee.getId(),
                trainee.getFirstName(),
                trainee.getLastName(),
                trainee.getRating());
    }

    @Override
    public String visitSubject(Subject subject) {
        return String.format("delete from subject where id = %d and name = %s",
                subject.getId(),
                subject.getName());
    }

    @Override
    public String visitSchool(School school) {
        return String.format("delete from school where id = %d and name = %s and year = %d",
                school.getId(),
                school.getName(),
                school.getYear());
    }

    @Override
    public String visitGroup(Group group) {
        return String.format("delete from `group` where id = %d and name = %s and room = %s",
                group.getId(),
                group.getName(),
                group.getRoom());
    }
}
